'use strict'
const request = require('supertest')
const app = require('../src/app.js')
jest.useFakeTimers()

describe('Test webserver', () => {

    beforeAll(done => {
        app.close(done)
    })

    describe('POST /add', () => {
        it('It should POST an event', (done) => {
            request(app)
                .post('/add')
                .send({title:'d', description:' dd', place:'paris', date:'2021-12-02', begin:'20:36', end:'20:36'})
                .expect(302)
                .end((err, res) => {
                    if (err) {
                        return done(err)
                    }
                    done()
                })
        })

        it('It should POST an event', (done) => {
            request(app)
                .post('/add')
                .send('title=Titre de test')
                .set('Accept', 'application/x-www-form-urlencoded')
                .expect(302)
                .end((err, res) => {
                    if (err) {
                        return done(err)
                    }
                    done()
                })
        })

        // it('It should POST an event', (done) => {
        //     request(app)
        //         .get('/events/1')
        //         // .send('title=Titre de test')
        //         // .set('Accept', 'application/x-www-form-urlencoded')
        //         .expect(200)
        //         .end((err, res) => {
        //             if (err) {
        //                 return done(err)
        //             }
        //             done()
        //         })
        // })
    })

})