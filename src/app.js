'use strict'
const express = require('express')
const app = express()
const morgan = require('morgan')
const port = 8082
app.use(morgan('dev'))
app.use(express.json())
const nunjucks = require('nunjucks')
const bunyan = require('bunyan')
const log = bunyan.createLogger({name:String})
const fs = require('fs')
const {readFile} = require('fs').promises
const eventsPath = 'data/events.json'
app.use(express.static('src/public'))
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

nunjucks.configure('src/views', {
    autoescape: true,
    express: app
})

app.get('/', (req, res) => {
    res.render('index.html', {home:{heading:'events'}})
})

app.get('/add', (req, res, next) => {
    res.render('add.html', {home:{
        heading:'Events',
        events:'ajout',
        date:new Date(),
    }})
})

app.get('/events', (req, res, next) => {
    getEventsData().then((obj) => {
        res.render('home.html', {home:{
            heading:'Events',
            events:obj,
        }})
    })
})

app.post('/add', (req, res) => {
    getEventsData().then((obj) => {
        const idEvent = Date.now()
        const newEvents = {
            id:idEvent,
            ...req.body
        }
        obj.push(newEvents)
        saveEventsData(obj)
        res.status(200).redirect('/events')
    })
})
app.get('/events/:id', (req, res) => {
    getEventsData().then((obj) => {
        const id = parseInt(req.params.id)
        const event = obj.find(ev => ev.id === id)
        res.render('homeId.html', {homeId:{
            heading:'Event',
            events:event
        }})

    })

})

app.get('/eventsedit/:id', (req, res) => {
    getEventsData().then((obj) => {
        const id = parseInt(req.params.id)
        const event = obj.find(ev => ev.id === id)
        res.render('edit.html', {edit:{
            heading:'Event',
            events:event,
            date:new Date()
        }})

    })

})

// put
app.post('/edit/:id', (req, res) => {
    const idOlde = parseInt(req.params.id)
    getEventsData().then((obj) => {
        const filterEvents = obj.filter(idEvent => idEvent.id !== idOlde)
        saveEventsData(filterEvents)
        const newEvents = {
            id:idOlde,
            ...req.body
        }
        obj.push(newEvents)
        saveEventsData(obj)
        res.status(200).redirect('/events')
    })
})

//delete
app.get('/events/delete/:id', (req, res) => {
    getEventsData().then((obj) => {
        const id = parseInt(req.params.id)
        const allEvents = obj
        //filter the event to remove it
        const filterEvents = allEvents.filter(idEvent => idEvent.id !== id)
        //console.log(filterEvents)
        if (allEvents.length === filterEvents.length) {
            return res.status(404).send('event does not exist')
        }
        //save the filtered data
        saveEventsData(filterEvents)
        res.status(200).redirect('/events')
    })

})

const saveEventsData = (data) => {
    const stringifyData = JSON.stringify(data)
    fs.writeFile(eventsPath, stringifyData, (err) => {
        if (err) {
            throw err
        }
        log.info('The file has been saved!')
    })
}

// async-promise-then
const getEventsData = () => {
    return readFile(eventsPath, 'utf8')
        .then(JSON.parse)
        .then((obj) => {
            //log.info(obj)
            return obj
        })
        .catch((err) => {
            if (err instanceof SyntaxError) {
                log.error('Invalid JSON in file', err)
            } else {
                log.error('Unknown error', err)
            }
        })
}

module.exports = app.listen(port, () => {
    log.info(`App listening at http://localhost:${port}`)
})