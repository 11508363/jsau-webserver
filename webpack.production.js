'use strict'

const {merge} = require('webpack-merge')
const common = require('./webpack.common.js')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = merge(common, {
    mode: 'production',
    output: {
        clean: true, // Clean the output directory before emitting
    },
    plugins: [
        // GZip compression
        new CompressionPlugin({
            test: /\.(js|css)$/,
            filename: '[path][base].gz',
        }),
        new CompressionPlugin({
            test: /\.(js|css)$/,
            algorithm: 'brotliCompress',
            filename: '[path][base].br',
        }),
    ],
})
